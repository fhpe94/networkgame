package game2018;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.*;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.text.*;

public class Main extends Application {

	public static final int size = 40;
	public static final int scene_height = size * 20 + 100;
	public static final int scene_width = size * 20 + 200;

	public static Image image_floor;
	public static Image image_wall;
	public static Image hero_right, hero_left, hero_up, hero_down;

	public static Player me;
	public static List<Player> players = new ArrayList<>();

	private Label[][] fields;
	private TextArea scoreList;

	private TreeSet<Message> messageQueue = new TreeSet<>();

	private int myId = 0;

	private String[] board = { // 20x20
			"wwwwwwwwwwwwwwwwwwww", "w        ww        w", "w w  w  www w  w  ww", "w w  w   ww w  w  ww",
			"w  w               w", "w w w w w w w  w  ww", "w w     www w  w  ww", "w w     w w w  w  ww",
			"w   w w  w  w  w   w", "w     w  w  w  w   w", "w ww ww        w  ww", "w  w w    w    w  ww",
			"w        ww w  w  ww", "w         w w  w  ww", "w        w     w  ww", "w  w              ww",
			"w  w www  w w  ww ww", "w w      ww w     ww", "w   w   ww  w      w", "wwwwwwwwwwwwwwwwwwww" };

	// -------------------------------------------
	// | Maze: (0,0) | Score: (1,0) |
	// |-----------------------------------------|
	// | boardGrid (0,1) | scorelist |
	// | | (1,1) |
	// -------------------------------------------

        // Udkommenter egen ip.
        //playerIps.put("Flemming", "10.24.20.98");
        playerIps.put("Mikkel", "10.24.20.239");
        playerIps.put("Matth", "10.24.84.150");

	@Override
	public void start(Stage primaryStage) {

		// Udkommenter egen ip.
		// playerIps.put("Flemming", "10.24.20.98");
		playerIps.put("Mikkel", "10.24.20.239");
		playerIps.put("Matth", "10.24.84.150");

		(new SocketHandler(this)).start();

		try {
			GridPane grid = new GridPane();
			grid.setHgap(10);
			grid.setVgap(10);
			grid.setPadding(new Insets(0, 10, 0, 10));

			Text mazeLabel = new Text("Maze:");
			mazeLabel.setFont(Font.font("Arial", FontWeight.BOLD, 20));

			Text scoreLabel = new Text("Score:");
			scoreLabel.setFont(Font.font("Arial", FontWeight.BOLD, 20));

			scoreList = new TextArea();

			GridPane boardGrid = new GridPane();

			image_wall = new Image(getClass().getResourceAsStream("Image/wall4.png"), size, size, false, false);
			image_floor = new Image(getClass().getResourceAsStream("Image/floor1.png"), size, size, false, false);

			hero_right = new Image(getClass().getResourceAsStream("Image/heroRight.png"), size, size, false, false);
			hero_left = new Image(getClass().getResourceAsStream("Image/heroLeft.png"), size, size, false, false);
			hero_up = new Image(getClass().getResourceAsStream("Image/heroUp.png"), size, size, false, false);
			hero_down = new Image(getClass().getResourceAsStream("Image/heroDown.png"), size, size, false, false);

			fields = new Label[20][20];
			for (int j = 0; j < 20; j++) {
				for (int i = 0; i < 20; i++) {
					switch (board[j].charAt(i)) {
					case 'w':
						fields[i][j] = new Label("", new ImageView(image_wall));
						break;
					case ' ':
						fields[i][j] = new Label("", new ImageView(image_floor));
						break;
					default:
						throw new Exception("Illegal field value: " + board[j].charAt(i));
					}
					boardGrid.add(fields[i][j], i, j);
				}
			}
			scoreList.setEditable(false);

			grid.add(mazeLabel, 0, 0);
			grid.add(scoreLabel, 1, 0);
			grid.add(boardGrid, 0, 1);
			grid.add(scoreList, 1, 1);

			Scene scene = new Scene(grid, scene_width, scene_height);
			primaryStage.setScene(scene);
			primaryStage.show();

            me = new Player("Flemming", 11, 7, "up");
            players.add(me);
            fields[11][7].setGraphic(new ImageView(hero_up));

			// Setting up standard players

			me = new Player("Flemming", 11, 7, "up");
			players.add(me);
			fields[11][7].setGraphic(new ImageView(hero_up));

			scoreList.setText(getScoreList());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

    public void playerMoved(int delta_x, int delta_y, String direction) {
    	try {
			TimeUnit.MILLISECONDS.sleep(50);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
        me.direction = direction;
        int x = me.getXpos(), y = me.getYpos();
        System.out.println(x + "," + y);
        int oldx = x;
        int oldy = y;

	public void playerMoved(int delta_x, int delta_y, String direction) {
		try {
			TimeUnit.MILLISECONDS.sleep(50);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		me.direction = direction;
		int x = me.getXpos(), y = me.getYpos();
		int oldx = x;
		int oldy = y;

		if (board[y + delta_y].charAt(x + delta_x) == 'w') {
			me.addPoints(-1);
			sendPoints(me.name, me.point);
		} else {
			Player p = getPlayerAt(x + delta_x, y + delta_y);
			if (p != null) {
				me.addPoints(10);
				sendPoints(me.name, me.point);
				p.addPoints(-10);
				sendPoints(p.name, p.point);
			} else {
				me.addPoints(1);
				sendPoints(me.name, me.point);

				fields[x][y].setGraphic(new ImageView(image_floor));
				x += delta_x;
				y += delta_y;

				sendMove(oldx, oldy, direction);
				if (direction.equals("right")) {
					fields[x][y].setGraphic(new ImageView(hero_right));
				}
				if (direction.equals("left")) {
					fields[x][y].setGraphic(new ImageView(hero_left));
				}
				if (direction.equals("up")) {
					fields[x][y].setGraphic(new ImageView(hero_up));
				}
				if (direction.equals("down")) {
					fields[x][y].setGraphic(new ImageView(hero_down));
				}

				me.setXpos(x);
				me.setYpos(y);
			}
		}
		scoreList.setText(getScoreList());
	}

	private void sendPoints(String name, int point) {
		Message temp = new Message(myId, 0, "POINT " + name + "," + point);
		sendMessage(temp);
	}

	private void sendMessage(Message message) {
		for (String ip : playerIps.values()) {
			try {
				Socket clientSocket = new Socket(ip, 5678);
				DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());
				String sentence = players.get(0).toSentenceString();
				System.out.println(sentence);
				outToServer.writeBytes(message + "\n");
				outToServer.flush();
				clientSocket.close();
			} catch (Exception e) {

			}
		}
	}

	private void sendMove(int x, int y, String dir) {
		String message = String.format("%s %s,%s,%s", "MOVE", x, y, dir);
		Message temp = new Message(myId, 0, message);
		sendMessage(temp);
	}

	public String getScoreList() {
		StringBuffer b = new StringBuffer(100);
		for (Player p : players) {
			b.append(p + "\r\n");
		}
		return b.toString();
	}

	public Player getPlayerAt(int x, int y) {
		for (Player p : players) {
			if (p.getXpos() == x && p.getYpos() == y) {
				return p;
			}
		}
		return null;
	}

	public static void main(String[] args) throws IOException {
		launch(args);
		System.exit(0);// Not to be used in final product!!!!!!
	}

	public void drawPlayers() {
		for (Player p : players) {
			Image temp;
			switch (p.getDirection()) {
			case "up":
				temp = hero_up;
				break;
			case "down":
				temp = hero_down;
				break;
			case "left":
				temp = hero_left;
				break;
			case "right":
				temp = hero_right;
				break;
			default:
				temp = hero_up;
			}
			fields[p.getXpos()][p.getYpos()].setGraphic(new ImageView(temp));
		}
	}

	public CommThread createCommThread(Socket connSock) {
		return new CommThread(connSock);
	}

	public class ExecThread extends Thread {

		@Override
		public void run() {
			while (true) {
				if (!messageQueue.isEmpty()) {
					executeMsg(messageQueue.pollFirst());
				}
			}
		}

		private synchronized void executeMsg(Message msg) {
			System.out.println("EXECUTE: " + msg);
		}

	}

	public class CommThread extends Thread {

		Socket connSocket;

		public CommThread(Socket connSocket) {
			this.connSocket = connSocket;
		}

		private Message stringToMessage(String message) {
			String parts[] = message.split(";");
			String msg = parts[0];
			int id = Integer.parseInt(parts[1].trim());
			int time = Integer.parseInt(parts[2].trim());
			return new Message(id, time, msg);
		}

		@Override
		public void run() {
			try {
				BufferedReader inFromClient = new BufferedReader(new InputStreamReader(connSocket.getInputStream()));
				boolean connected = true;
				while (connected) {
					String clientSentence = inFromClient.readLine();

					if (clientSentence == null) {
						connected = false;
					} else {
						Message msg = stringToMessage(clientSentence);
						System.out.println("Insert To Queue");
						messageQueue.add(msg);
						System.out.println("Inserted to Queue");
						if (clientSentence.startsWith("NAME")) {
							addRemotePlayer(clientSentence);
						}
						if (clientSentence.startsWith("MOVE")) {
							handleMove(clientSentence);
						}
						if (clientSentence.startsWith("POINT")) {
							handlePoints(clientSentence);
						}
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		private void handlePoints(String information) {
			information = information.replace("POINT", "").trim();
			String info[] = information.split(",");
			String name = info[0];
			int point = Integer.parseInt(info[1]);
			Platform.runLater(() -> {
				for (Player p : players) {
					if (p.name.equals(name)) {
						p.point = point;
					}
				}
				scoreList.setText(getScoreList());
			});

		}

		private void handleMove(String information) {
			information = information.replace("MOVE", "").trim();
			String info[] = information.split(",");
			int x = Integer.parseInt(info[0]);
			int y = Integer.parseInt(info[1]);
			String dir = info[2];
			Player temp = getPlayerAt(x, y);

			if (temp != null) {
				Platform.runLater(() -> {
					Image tempImg = null;
					switch (dir) {
					case "up":
						tempImg = hero_up;
						temp.setYpos(y - 1);
						break;
					case "down":
						tempImg = hero_down;
						temp.setYpos(y + 1);
						break;
					case "left":
						tempImg = hero_left;
						temp.setXpos(x - 1);
						break;
					case "right":
						tempImg = hero_right;
						temp.setXpos(x + 1);
						break;
					}
					fields[x][y].setGraphic(new ImageView(image_floor));
					fields[temp.getXpos()][temp.getYpos()].setGraphic(new ImageView(tempImg));
				});
			}
		}

		private void addRemotePlayer(String information) {
			information = information.replace("NAME", "").trim();
			String info[] = information.split(",");
			Player temp = new Player(info[0], Integer.parseInt(info[1]), Integer.parseInt(info[2]), info[3]);

			if (!players.contains(temp)) {
				Main.players.add(temp);

				Platform.runLater(() -> {
					Image tempImg;
					switch (temp.direction) {
					case "up":
						tempImg = hero_up;
						break;
					case "down":
						tempImg = hero_down;
						break;
					case "left":
						tempImg = hero_left;
						break;
					case "right":
						tempImg = hero_right;
						break;
					default:
						tempImg = hero_up;
					}
					fields[temp.getXpos()][temp.getYpos()].setGraphic(new ImageView(tempImg));
				});
			}
		}

	}
}

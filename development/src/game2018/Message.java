package game2018;

public class Message implements Comparable<Message> {
	private int id;
	private int timestamp;
	private String message;
	
	public Message(int id, int timestamp, String message) {
		this.id = id;
		this.timestamp = timestamp;
		this.message = message;
	}
	
	@Override
	public String toString() {
		return String.format("%s, %s, %s", message, id, timestamp);
	}

	@Override
	public int compareTo(Message other) {
		if(this.timestamp == other.timestamp) {
			return this.id - other.id;
		}else {
			return this.timestamp - other.timestamp;
		}
	}
	
	
}

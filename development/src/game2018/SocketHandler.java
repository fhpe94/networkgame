package game2018;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import javafx.stage.Stage;

public class SocketHandler extends Thread {
	private Main main;
	
	public SocketHandler(Main main) {
		this.main = main;
	}
	
	@Override
	public void run() {
		System.out.println("setting server");
		try {
			ServerSocket welcomeSocket = new ServerSocket(5678);
			while (true) {
				Socket connectionSocket = welcomeSocket.accept();
				(main.createCommThread(connectionSocket)).start();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}

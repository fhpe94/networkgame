package game2018;

public class Player {
	String name;
	int xpos;
	int ypos;
	int point;
	String direction;

	public Player(String name, int xpos, int ypos, String direction) {
		this.name = name;
		this.xpos = xpos;
		this.ypos = ypos;
		this.direction = direction;
		this.point = 0;
	}

	public int getXpos() {
		return xpos;
	}
	public void setXpos(int xpos) {
		this.xpos = xpos;
	}
	public int getYpos() {
		return ypos;
	}
	public void setYpos(int ypos) {
		this.ypos = ypos;
	}
	public String getDirection() {
		return direction;
	}
	public void setDirection(String direction) {
		this.direction = direction;
	}
	public void addPoints(int p) {
		point+=p;
	}
	
	public String toSentenceString() {
		return name + "," +  xpos + "," +ypos+"," + direction;
	}
	
	public String toString() {
		return name+":   "+point;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(this.name.equals(((Player) obj).name)) {
			return true;
		}else {
			return false;
		}
	}
	
	@Override
	public int hashCode() {
		return name.hashCode();
	}
}
